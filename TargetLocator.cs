using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLocator : MonoBehaviour
{
    [SerializeField] private Transform wepon;
    [SerializeField] private float range = 15f;
    [SerializeField] private ParticleSystem projectileParticales;

    Transform target;


    void Update()
    {
        FindClosetTarget();
        AimWeapon();
    }

    void FindClosetTarget()
    {
        Enermy[] enermies = FindObjectsOfType<Enermy>();
        Transform closetTarget = null;
        float maxDistance = Mathf.Infinity;

        foreach (Enermy enermy in enermies)
        {
            float targetDistance = Vector3.Distance(transform.position, enermy.transform.position);
            if (targetDistance < maxDistance)
            {
                closetTarget = enermy.transform;
                maxDistance = targetDistance;
            }

            target = closetTarget;
        }
    }
    void AimWeapon()
    {
        float targetDistance = Vector3.Distance(transform.position, target.position);
        
        wepon.LookAt(target);

        if (targetDistance < range)
        {
            Attack(true);
        }
        else
        {
            Attack(false);
        }
    }

    void Attack(bool isActive)
    {
        var emissionModule = projectileParticales.emission;
        emissionModule.enabled = isActive;
    }
}