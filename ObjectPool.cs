using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject enermyPrefab;
    [SerializeField] [Range(0,50)] int poolSize = 5;
    [SerializeField] [Range(0.1f, 30f)] float spawTimer = 1f;

    GameObject[] pool;
    void Awake()
    {
        PopulatePool();
    }

    void Start()
    {
        StartCoroutine(SpawEnermy());
    }

    void PopulatePool()
    {
        pool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            pool[i] = Instantiate(enermyPrefab, transform);
            pool[i].SetActive(false);
        }
    }

    void OnEnableObjectInPool()
    {
        for (int i = 0; i < pool.Length; i++)
        {
            if (pool[i].activeInHierarchy == false)
            {
                pool[i].SetActive(true);
                return; 
            }
        }
    }
    IEnumerator SpawEnermy()
    {
        while (true)
        {
            OnEnableObjectInPool();
            yield return new WaitForSeconds(spawTimer);
        }
    }
}
