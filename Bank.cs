using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Bank : MonoBehaviour
{
    [SerializeField] int startingBalance = 300;
    [SerializeField] int currentBalence;
    [SerializeField] TextMeshProUGUI displayBalence;
    public int CurrentBalence { get { return currentBalence;}}

    private void Awake()
    {
        currentBalence = startingBalance;
        UpdateDisplay();
    }

    public void Desposit(int amount)
    {
        currentBalence +=Mathf.Abs(amount);
        UpdateDisplay();
    }

    public void Withdraw(int amount)
    {
        currentBalence -= Mathf.Abs(amount);
        UpdateDisplay();

        if (currentBalence < 0)
        {
            ReloadScene();
        }
    }

    void UpdateDisplay()
    {
        displayBalence.text = " Gold: " + currentBalence;
    }

    void ReloadScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }
}
