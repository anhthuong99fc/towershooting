using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnermyHealth : MonoBehaviour
{
    [SerializeField] int maxHitPoints = 5;

    [Tooltip("Add amount to maxHitPoints when enermy die.")]
    [SerializeField] private int difficultyRamp = 1;

    int currentHitPoint = 0;
    Enermy enermy;
    void OnEnable()
    {
        currentHitPoint = maxHitPoints;
    }

    void Start()
    {
        enermy = GetComponent<Enermy>();
    }

    void OnParticleCollision(GameObject other)
    {
        ProcessHit();
    }
    void ProcessHit()
    {
        currentHitPoint--;
        if (currentHitPoint <= 0)
        {
            gameObject.SetActive(false);
            maxHitPoints += difficultyRamp;
            enermy.RewardGold();
        }
    }
}
